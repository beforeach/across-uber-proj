## Fetching all submodules with:

~~~~
git submodule update --init --recursive
~~~~

## In case you need to re-add the submodules and start from scratch
~~~~
git submodule add https://bitbucket.org/beforeach/across.git
git submodule add https://bitbucket.org/beforeach/across-autoconfigure.git
git submodule add https://bitbucket.org/beforeach/across-hibernate-module.git
git submodule add https://bitbucket.org/beforeach/spring-security-module.git
git submodule add https://bitbucket.org/beforeach/debug-web-module.git
git submodule add https://bitbucket.org/beforeach/logging-module.git
git submodule add https://bitbucket.org/beforeach/ehcache-module.git
git submodule add https://bitbucket.org/beforeach/spring-mobile-module.git
git submodule add https://bitbucket.org/beforeach/application-info-module.git
git submodule add https://bitbucket.org/beforeach/bootstrap-ui-module.git
git submodule add https://bitbucket.org/beforeach/admin-web-module.git
git submodule add https://bitbucket.org/beforeach/entity-ui-module.git
git submodule add https://bitbucket.org/beforeach/spring-security-acl-module.git
git submodule add https://bitbucket.org/beforeach/properties-module.git
git submodule add https://bitbucket.org/beforeach/user-module.git
git submodule add https://bitbucket.org/beforeach/oauth2-module.git
git submodule add https://bitbucket.org/beforeach/ldap-module.git
git submodule add https://bitbucket.org/beforeach/spring-batch-module.git
git submodule add https://bitbucket.org/beforeach/file-manager-module.git
git submodule add https://bitbucket.org/beforeach/dynamic-forms-module.git
git submodule add https://bitbucket.org/beforeach/web-cms-module.git
~~~~